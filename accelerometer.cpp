#include "BlackLib.h"
#include "BlackI2C/BlackI2C.h"
#include <string>
#include <iostream>
#include <unistd.h>
using namespace BlackLib;

int usecs_to_secs( float seconds ){ return int(seconds * 1000000); }

int main()
{

    BlackI2C  myI2c(I2C_2, 0x1D);
    bool isOpened = myI2c.open( ReadWrite | NonBlock );

    if( !isOpened )
    {
        std::cout << "I2C_2 DEVICE CAN\'T OPEN.;" << std::endl;
        exit(1);
    }

    // read-write register
    uint8_t powerCtl_Addr  = 0x16;
    uint8_t offsetX_LSB    = 0x10;
    uint8_t offsetX_MSB    = 0x11;
    uint8_t offsetY_LSB    = 0x12;
    uint8_t offsetY_MSB    = 0x13;
    uint8_t offsetZ_LSB    = 0x14;
    uint8_t offsetZ_MSB    = 0x15;

    // read only register
    uint8_t axisX_Addr     = 0x06;
    uint8_t axisY_Addr     = 0x07;
    uint8_t axisZ_Addr     = 0x08;

    // set bit
    uint8_t measureMode     = 0x55;

    uint8_t powerCtlReg     = myI2c.readByte(powerCtl_Addr);

    std::cout << "Power Ctrl's current value: " << std::hex << (int)powerCtlReg << std::dec << std::endl;

    /*powerCtlReg |= (measureMode);      //                      ___ ___ ___ ___|___ ___ ___ ___
                                       // powerCtlReg:        |_x_|_x_|_x_|_x_|_x_|_x_|_x_|_x_|
                                       // measureMode:        |_0_|_0_|_0_|_0_|_1_|_0_|_0_|_0_|
                                       //                      ___ ___ ___ ___|___ ___ ___ ___ or these
                                       // result:             |_x_|_x_|_x_|_x_|_1_|_x_|_x_|_x_|
    
    */   

    bool resultOfWrite      = myI2c.writeByte(powerCtl_Addr, measureMode);
    powerCtlReg             = myI2c.readByte(powerCtl_Addr);

    //Calibracion
    resultOfWrite      = myI2c.writeByte(offsetX_LSB, 0x00);
    resultOfWrite      = myI2c.writeByte(offsetX_MSB, 0x00);
    resultOfWrite      = myI2c.writeByte(offsetY_LSB, 0x00);
    resultOfWrite      = myI2c.writeByte(offsetY_MSB, 0x00);
    resultOfWrite      = myI2c.writeByte(offsetZ_LSB, 0x00);
    resultOfWrite      = myI2c.writeByte(offsetZ_MSB, 0x00);

    std::cout << "Power Ctrl's new value: " << std::hex << (int)powerCtlReg << std::dec << std::endl;

    while(true)
    {   
        int x_value;
        int y_value;
        int z_value;

        x_value = myI2c.readByte(axisX_Addr);
        y_value = myI2c.readByte(axisY_Addr);
        z_value = myI2c.readByte(axisZ_Addr);
        std::cout << "X -> : " << std::dec << (int)x_value << std::dec << std::endl;
        std::cout << "Y -> : " << std::dec << (int)y_value << std::dec << std::endl;
        std::cout << "Z -> : " << std::dec << (int)z_value << std::dec << std::endl;
        std::cout << "----------------------\n";
        usleep(usecs_to_secs(0.5));
    }

    return 0;
}