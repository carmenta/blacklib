#include"BlackGPIO/BlackGPIO.h"
#include <unistd.h>
using namespace BlackLib;
//using namespace std;
 
int main(void) {
BlackGPIO PIN1(GPIO_67,output, SecureMode);
BlackGPIO PIN2(GPIO_68,output, SecureMode);
BlackGPIO PIN3(GPIO_44,output, SecureMode);
BlackGPIO PIN4(GPIO_26,output, SecureMode);
BlackGPIO PIN5(GPIO_46,output, SecureMode);
BlackGPIO PIN6(GPIO_65,output, SecureMode); 
BlackGPIO PINx(GPIO_67,output, SecureMode);
 
for(;;){
PINx.setValue(high);
PIN1.setValue(high);
PIN2.setValue(high);
PIN3.setValue(high);
PIN4.setValue(high);
PIN5.setValue(high);
PIN6.setValue(high);
usleep(10000);
PINx.setValue(low);
PIN1.setValue(low);
PIN2.setValue(low);
PIN3.setValue(low);
PIN4.setValue(low);
PIN5.setValue(low);
PIN6.setValue(low);
usleep(10000);
}
}
